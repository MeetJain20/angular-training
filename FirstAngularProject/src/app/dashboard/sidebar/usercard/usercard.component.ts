import { Component, EventEmitter, Input, Output } from '@angular/core';
import { User } from '../../../interface';
import { NgFor } from '@angular/common';
import { NgIf } from '@angular/common';
import { DataService } from '../../../data.service';
import { NgClass } from '@angular/common';
import { DatePipe } from '@angular/common';
import { CountrycodePipe } from './countrycode.pipe';
@Component({
  selector: 'app-usercard',
  standalone: true,
  imports: [NgFor, NgClass, NgIf, DatePipe,CountrycodePipe],
  templateUrl: './usercard.component.html',
  styleUrl: './usercard.component.scss',
})
export class UsercardComponent {
  @Input() user: User = {
    id: 0,
    profileimage:
      'https://img.freepik.com/premium-vector/vector-flat-man-avatar-no-face-with-hairstyle_574806-4740.jpg',
    name: '',
    dob: new Date(1900 - 1 - 1),
    mobile: 0,
    age: 0,
    role: 'User',
    country: 'India',
  };
  @Input() selectedUser1: User = {
    id: 0,
    profileimage:
      'https://img.freepik.com/premium-vector/vector-flat-man-avatar-no-face-with-hairstyle_574806-4740.jpg',
    name: '',
    dob: new Date(1900 - 1 - 1),
    mobile: 0,
    age: 0,
    role: 'User',
    country: 'India',
  };
  constructor(private dataService: DataService) {}

  @Output() sendSelectedUser = new EventEmitter<User>();

  selectedUser() {
    // Emit data to parent
    this.sendSelectedUser.emit(this.user);
  }
  
  @Output() deletefromside = new EventEmitter<User>();

  delete() {
    // Emit data to parent
    this.deletefromside.emit(this.user);
  }
  
}
