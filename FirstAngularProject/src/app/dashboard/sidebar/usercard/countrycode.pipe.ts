import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../../../interface';

@Pipe({
  name: 'countrycode',
  standalone: true
})
export class CountrycodePipe implements PipeTransform {

  transform(user: User): string {
    let countryCode = '';
    switch (user.country) {
      case 'India':
        countryCode = '+91';
        break;
      case 'Thailand':
        countryCode = '+66';
        break;
      case 'Australia':
        countryCode = '+61';
        break;
      case 'Japan':
        countryCode = '+81'
        break;
      // Add more cases for other countries as needed
      default:
        countryCode = ''; // Default country code
    }
    return `${countryCode} ${user.mobile}`;
  }
}
