import { Component,EventEmitter,Input, Output } from '@angular/core';
import { UsercardComponent } from './usercard/usercard.component';
import { User } from '../../interface';
import { NgFor } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SearchFilterPipe } from './search-filter.pipe';
@Component({
  selector: 'app-sidebar',
  standalone: true,
  imports: [UsercardComponent,NgFor,SearchFilterPipe,FormsModule],
  templateUrl: './sidebar.component.html',
  styleUrl: './sidebar.component.scss'
})
export class SidebarComponent {
  @Input() users: User[] = [];
  @Input() user: User = {
    id: 0,
    profileimage:
      'https://img.freepik.com/premium-vector/vector-flat-man-avatar-no-face-with-hairstyle_574806-4740.jpg',
    name: '',
    dob:new Date(1900-1-1),
    mobile: 0,
    age: 0,
    role:'User',country:'India'
  };

  searchTerm:string="";

  selectedData: any;
  selectedUser(data: any) {
    // Handle data received from child
    this.selectedData = data;
    // this.users.push(this.selectedData);
  }

  deleteData: User={
    id: 0,
    profileimage:
      'https://img.freepik.com/premium-vector/vector-flat-man-avatar-no-face-with-hairstyle_574806-4740.jpg',
    name: '',
    dob:new Date(1900-1-1),
    mobile: 0,
    age: 0,
    role:'User',country:'India'
  };
  delete(data: User) {
    // Handle data received from child
    this.deleteData = data;
    // this.users.push(this.selectedData);
  }

  @Output() sendSelectedUser = new EventEmitter<User>();

  sendUserData() {
    // Emit data to parent
    this.sendSelectedUser.emit(this.selectedData);
  }

  @Output() deletesend1 = new EventEmitter<User>();

  deletesend() {
    // Emit data to parent
    this.deletesend1.emit(this.deleteData);
  }
  
}
