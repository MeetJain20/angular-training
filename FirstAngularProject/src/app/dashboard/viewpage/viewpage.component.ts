import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { User } from '../../interface';
import { NgIf } from '@angular/common';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-viewpage',
  standalone: true,
  imports: [FormsModule, NgIf,DatePipe],
  templateUrl: './viewpage.component.html',
  styleUrls: ['./viewpage.component.scss'],
})
export class ViewpageComponent implements OnInit {
  isCorrectName: Boolean = true;
  isCorrectDob: Boolean = true;
  isCorrectMobile: Boolean = true;
  isCorrectAge: Boolean = true;
  isEditable: Boolean = false;
  isEditable1: Boolean = false;
  uniqueId: number = 0;
  @Input() user: User = {
    id: 0,
    profileimage:
      'https://img.freepik.com/premium-vector/vector-flat-man-avatar-no-face-with-hairstyle_574806-4740.jpg',
    name: '',
    dob:new Date(1900-1-1),
    mobile: 0,
    age: 0,
    role: 'User',country:'India'
  };
  @Input() addNewUser: Boolean = false;

  isUserSelected: Boolean = false;
  receivedData: any;

  formData: any = {
    id: 0,
    profileimage: '',
    name: '',
    dob:new Date(1900-1-1),
    mobile: 0,
    age: 0,
    role: 'User',country:'India'
  };

  ngOnInit() {
    this.uniqueId = Math.floor(Math.random() * (1000000 - 1 + 1)) + 1;
    this.updateFormData();
  }

  ngOnChanges(changes: SimpleChanges) {
    if ((changes['user'] && changes['user'].currentValue)) {
      this.updateFormData();
    }
  }

  updateFormData() {
    if (this['user'] && this['user'].name !== '') {
      this.isCorrectName = true;
      this.isCorrectMobile = true;
      this.isCorrectAge = true;
      this.isCorrectDob = true;
      this.isUserSelected = true;
      this.isEditable=false;
      this.addNewUser = true;
      this.isEditable1 = false;
      this.formData = {
        id: this['user'].id || this.uniqueId,
        profileimage:
          this['user'].profileimage ||
          'https://img.freepik.com/premium-vector/vector-flat-man-avatar-no-face-with-hairstyle_574806-4740.jpg',
        name: this['user'].name || '',
        dob:this['user'].dob|| new Date(1900-1-1),
        mobile: this['user'].mobile || 0,
        age: this['user'].age || 0,
        role: this['user'].role || 'User',
        country: this['user'].country || 'India',
      };
    } else {
      this.isCorrectName = true;
      this.isCorrectMobile = true;
      this.isCorrectAge = true;
      this.isCorrectDob = true;
      this.isUserSelected = false;
      this.isEditable=true;
      this.addNewUser = false;
      this.formData = {
        id: this.uniqueId,
        profileimage:
          'https://img.freepik.com/premium-vector/vector-flat-man-avatar-no-face-with-hairstyle_574806-4740.jpg',
        name: '',
        dob:new Date(1900-1-1),
        mobile: 0,
        age: 0,
        role: 'User',country:'India'
      };
    }
  }

  @Output() adduserFunc = new EventEmitter<User>();

  addUser() {
    const isNameValid = this.formData.name !== '';
    const isDateValid = this.formData.dob.toString() !== '';
    const isMobileValid = this.formData.mobile.toString().length === 10;
    const isAgeValid = this.formData.age > 0;
  
    if (isNameValid && isMobileValid && isAgeValid && isDateValid) {
      this.isUserSelected = false;
      this.isEditable = true;
      this.isEditable1 = false;
      // console.log(typeof this.formData.dob)
      this.adduserFunc.emit(this.formData);
      // this.addNewUser = false;
      this.formData = {
        id: this.uniqueId,
        profileimage: 'https://img.freepik.com/premium-vector/vector-flat-man-avatar-no-face-with-hairstyle_574806-4740.jpg',
        name: '',
        dob:new Date(1900-1-1),
        mobile: 0,
        age: 0,
        role:'User',country:'India'
      };
    } else {

      // Separate boolean variables for each field
      this.isCorrectName = isNameValid;
      this.isCorrectMobile = isMobileValid;
      this.isCorrectAge = isAgeValid;
      this.isCorrectDob = isDateValid;
    }
  }

  editUser() {
    this.isEditable = true;
    this.isEditable1 = true;
  }

  @Output() deleteuserFunc = new EventEmitter<User>();

  deleteUser() {
    this.addNewUser = false;

    this.formData = {
      id: this.uniqueId,
      profileimage:
        'https://img.freepik.com/premium-vector/vector-flat-man-avatar-no-face-with-hairstyle_574806-4740.jpg',
      name: '',
      dob: new Date(1970-1-1),
      mobile: 0,
      age: 0,
      role: 'User',country:'India'
    };
    this.deleteuserFunc.emit(this.user);
  }
}
