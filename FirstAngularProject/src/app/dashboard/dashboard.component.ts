import { Component, EventEmitter, Input, Output } from '@angular/core';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ViewpageComponent } from './viewpage/viewpage.component';
import { User } from '../interface';
import { FormsModule } from '@angular/forms';
@Component({
  selector: 'app-dashboard',
  standalone: true,
  imports: [SidebarComponent, ViewpageComponent, FormsModule],
  templateUrl: './dashboard.component.html',
  styleUrl: './dashboard.component.scss',
})
export class DashboardComponent {
  @Input() users: User[] = [];
  @Input() addNewUser: Boolean = false;
  @Input() user: User = {
    id: 0,
    profileimage: 'https://img.freepik.com/premium-vector/vector-flat-man-avatar-no-face-with-hairstyle_574806-4740.jpg',
    name: '',
    dob:new Date(1900-1-1),
    mobile: 0,
    age: 0,
    role:'User',country:'India'
  };

  userData: any;

  sendUserData(data: any) {
    // Handle data received from child
    // console.log(data);
    this.userData = data;
  }

  addedData: User={
    id: 0,
    profileimage: 'https://img.freepik.com/premium-vector/vector-flat-man-avatar-no-face-with-hairstyle_574806-4740.jpg',
    name: '',
    dob:new Date(1900-1-1),
    mobile: 0,
    age: 0,
    role:'User',country:'India'
  };

  addUser(data: User) {
    // Handle data received from child
    this.addedData = data;
    // this.users.push(this.addedData);
  }

  @Output() addedUser = new EventEmitter<User>();

  addUserDetails() {
    // Emit data to parent
    this.addedUser.emit(this.addedData);
  }

  deletedData: User={
    id: 0,
    profileimage: 'https://img.freepik.com/premium-vector/vector-flat-man-avatar-no-face-with-hairstyle_574806-4740.jpg',
    name: '',
    dob:new Date(1900-1-1),
    mobile: 0,
    age: 0,
    role:'User',country:'India'
  };

  deleteUser(data: User) {
    this.deletedData = data
  }

  deletedData1: User={
    id: 0,
    profileimage: 'https://img.freepik.com/premium-vector/vector-flat-man-avatar-no-face-with-hairstyle_574806-4740.jpg',
    name: '',
    dob:new Date(1900-1-1),
    mobile: 0,
    age: 0,
    role:'User',country:'India'
  };

  deletesend(data: User) {
    this.deletedData1 = data
  }

  @Output() deletedUser = new EventEmitter<User>();

  deleteUserDetails() {
    // Emit data to parent
    this.deletedUser.emit(this.deletedData);
  }

  @Output() deletedUser1 = new EventEmitter<User>();

  deleteUserDetails1() {
    // Emit data to parent
    this.deletedUser1.emit(this.deletedData1);
  }

  @Output() selectedUserDetails = new EventEmitter<User>();

  userDetails() {
    // Emit data to parent
    this.selectedUserDetails.emit(this.userData);
  }

}
