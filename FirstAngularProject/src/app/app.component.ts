import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterOutlet } from '@angular/router';
import { User } from './interface';
import { NavbarComponent } from './navbar/navbar.component';
import { DashboardComponent } from './dashboard/dashboard.component';
@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, FormsModule, NavbarComponent, DashboardComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent {
  title = 'FirstAngularProject';
  addNewUser: Boolean = false;
  userCount: number = 0;
  users: User[] = [
    {
      id: 1,
      profileimage:
        'https://img.freepik.com/premium-vector/vector-flat-man-avatar-no-face-with-hairstyle_574806-4740.jpg',
      name: 'John Doe',
      dob: new Date(2000 - 20 - 10),
      mobile: 1234567890,
      age: 30,
      role: 'Admin',
      country: 'India',
    },
    {
      id: 2,
      profileimage:
        'https://img.freepik.com/premium-vector/vector-flat-man-avatar-no-face-with-hairstyle_574806-4740.jpg',
      name: 'Jane Smith',
      dob: new Date(2000 - 20 - 10),
      mobile: 9876543210,
      age: 25,
      role: 'User',
      country: 'Thailand',
    },
    {
      id: 3,
      profileimage:
        'https://img.freepik.com/premium-vector/vector-flat-man-avatar-no-face-with-hairstyle_574806-4740.jpg',
      name: 'Alice Johnson',
      dob: new Date(2000 - 20 - 10),
      mobile: 4561237890,
      age: 35,
      role: 'User',
      country: 'India',
    },
    {
      id: 4,
      profileimage:
        'https://img.freepik.com/premium-vector/vector-flat-man-avatar-no-face-with-hairstyle_574806-4740.jpg',
      name: 'Bob Brown',
      dob: new Date(2000 - 20 - 10),
      mobile: 7894561230,
      age: 40,
      role: 'Admin',
      country: 'Australia',
    },
  ];

  user: User = {
    id: 5,
    profileimage:
      'https://img.freepik.com/premium-vector/vector-flat-man-avatar-no-face-with-hairstyle_574806-4740.jpg',
    name: '',
    dob: new Date(1900 - 1 - 1),
    mobile: 0,
    age: 0,
    role: 'User',
    country: 'India',
  };

  constructor() {
    this.userCount = this.users.length;
    const maxId = this.users.reduce(
      (max, user) => (user.id > max ? user.id : max),
      0
    );
    this.user.id = maxId + 1;
  }

  addedData: any;

  addUserDetails(data: any) {
    console.log(data);
    this.addedData = data;
    const existingUserIndex = this.users.findIndex(
      (user) => user.id === this.addedData.id
    );

    if (existingUserIndex !== -1) {
      // Update existing user
      this.users[existingUserIndex] = {
        ...this.users[existingUserIndex],
        ...this.addedData,
      };
    } else {
      // Add new user
      this.users.push(this.addedData);
      this.userCount = this.users.length;
    }
  }

  deletedData: User = {
    id: 0,
    profileimage:
      'https://img.freepik.com/premium-vector/vector-flat-man-avatar-no-face-with-hairstyle_574806-4740.jpg',
    name: '',
    dob: new Date(1900 - 1 - 1),
    mobile: 0,
    age: 0,
    role: 'User',
    country: 'India',
  };

  deleteUserDetails(data: User) {
    this.deletedData = data;
    const idToDelete = this.deletedData.id;
    const index = this.users.findIndex((user) => user.id === idToDelete);
    if (index !== -1) {
      this.users.splice(index, 1);
      this.userCount = this.users.length;
    }
  }

  deletedData1: User = {
    id: 0,
    profileimage:
      'https://img.freepik.com/premium-vector/vector-flat-man-avatar-no-face-with-hairstyle_574806-4740.jpg',
    name: '',
    dob: new Date(1900 - 1 - 1),
    mobile: 0,
    age: 0,
    role: 'User',
    country: 'India',
  };

  deleteUserDetails1(data: User) {
    this.deletedData1 = data;
    const idToDelete = this.deletedData1.id;
    const index = this.users.findIndex((user) => user.id === idToDelete);
    if (index !== -1) {
      this.users.splice(index, 1);
      this.userCount = this.users.length;
      this.user = {
        id: 0,
        profileimage:
          'https://img.freepik.com/premium-vector/vector-flat-man-avatar-no-face-with-hairstyle_574806-4740.jpg',
        name: '',
        dob: new Date(1900 - 1 - 1),
        mobile: 0,
        age: 0,
        role: 'User',
        country: 'India',
      };
    }
  }

  userDetails(data: User) {
    this.user = data;
  }

  showAddModal(data: Boolean) {
    // console.log(data);
    this.addNewUser = data;
  }
}
