
export interface User {
    id: number;
    profileimage:string;
    name: string;
    dob: Date;
    mobile: number;
    age: number; 
    role: string;  
    country:string;
  }