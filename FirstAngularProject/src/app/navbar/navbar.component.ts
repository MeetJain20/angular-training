import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NgClass } from '@angular/common';

@Component({
  selector: 'app-navbar',
  standalone: true,
  imports: [NgClass],
  templateUrl: './navbar.component.html',
  styleUrl: './navbar.component.scss',
})
export class NavbarComponent {
  addNewUser: Boolean = false;
  isActive: Boolean = false;
  isDisabled: Boolean = false;
  classObject = {
    active: this.isActive,
    disabled: this.isDisabled,
  };
  // @Input() userCount: number = 0;

  @Output() showaddmodal = new EventEmitter<Boolean>();

  showAddModal() {
    this.addNewUser = true;
    this.showaddmodal.emit(this.addNewUser);
  }

}
